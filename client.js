var pupil_error_text = "Your pupil diameter is too wide";
var ok_text = "OK";
var calibration_time = 3000;

function isJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

$(function() {
  var username = $('.logininfo:first').find('a:first').text();
  var title = document.title;
  if (!username) {
    username = "Anonymous"
  }

  var use_mouse = false;
  var ip = (document.location.protocol == "file:") ? 'localhost' : '192.168.100.100';

  var base_url = "http://" + ip + "/learningtest/eyetrack/"
  if (document.location.protocol == "file:") { base_url = "" };
  var port = 9001;
  var ws = new WebSocket("ws://" + ip + ":" + port + "/socket/");
  var menuHeight = 60;
  var target = '.target > *';
  var outerTarget = '.target';
  var last_inside_x, last_inside_y;
  var current_out_index;
  var current_in_index;

   boring_type_gaze_audio = new Audio(base_url + 'audio/boring_type_gaze.mp3');
   boring_type_pupil_audio = new Audio(base_url + 'audio/boring_type_pupil.mp3');
   gaze_music_audio = new Audio(base_url + 'audio/music.mp3');
   pupil_music_audio = new Audio(base_url + 'audio/music2.mp3');

   isPlaying = function(a) {
    return a.duration > 0 && !a.paused;
   }

  boring_type_gaze_audio.addEventListener('ended', function() {
      this.currentTime = 0;
      this.play();
  }, false);

  boring_type_pupil_audio.addEventListener('ended', function() {
      this.currentTime = 0;
      this.play();
  }, false);

  gaze_music_audio.addEventListener('ended', function() {
      this.currentTime = 0;
      this.play();
  }, false);

  pupil_music_audio.addEventListener('ended', function() {
      this.currentTime = 0;
      this.play();
  }, false);

  boring_type_gaze_audio.addEventListener('play', function() {
    boring_type_pupil_audio.pause();
    boring_type_pupil_audio.currentTime = 0;
    gaze_music_audio.pause();
    gaze_music_audio.currentTime = 0;
    pupil_music_audio.pause();
    pupil_music_audio.currentTime = 0;
  }, false);

  boring_type_pupil_audio.addEventListener('play', function() {
    boring_type_gaze_audio.pause();
    boring_type_gaze_audio.currentTime = 0;
    gaze_music_audio.pause();
    gaze_music_audio.currentTime = 0;
    pupil_music_audio.pause();
    pupil_music_audio.currentTime = 0;
  }, false);

  gaze_music_audio.addEventListener('play', function() {
    boring_type_gaze_audio.pause();
    boring_type_gaze_audio.currentTime = 0;
    boring_type_pupil_audio.pause();
    boring_type_pupil_audio.currentTime = 0;
    pupil_music_audio.pause();
    pupil_music_audio.currentTime = 0;
  }, false);

  pupil_music_audio.addEventListener('play', function() {
    boring_type_gaze_audio.pause();
    boring_type_gaze_audio.currentTime = 0;
    boring_type_pupil_audio.pause();
    boring_type_pupil_audio.currentTime = 0;
    gaze_music_audio.pause();
    gaze_music_audio.currentTime = 0;
  }, false);
  
  var sendBrowserData = function() {
    if ($(target).length > 0) {
      var x = window.screenX - window.scrollX + $(target).offset()['left'];
      var y = window.screenY - window.scrollY + $(target).offset()['top'] + menuHeight;
      var width = $(target).width();
      var height = $(target).height();
      ws.send(JSON.stringify({x:x, y:y, w:width, h:height}));
    } else {
      console.log("target element not found");
    }
  };


  ws.onerror = function() {
    console.log("Failed to open a connection");
  }

  ws.onmessage = function(message) {
    console.log(message.data);
    if (isJsonString(message.data)) {
      data = JSON.parse(message.data);
      var error_out = $('div.error_out');
      var error_in = $('div.error_in');

      if (data["last_inside_x"]) {
        last_inside_x = data["last_inside_x"];
      }
      if (data["last_inside_y"]) {
        last_inside_y = data["last_inside_y"];
      }
      // console.log(last_inside_x, last_inside_y);

      if (data["boring_type"] == 'gaze') {
        if (error_in.length != 0) {
          error_in.remove();
        }
        if (error_out.length == 0) {
          error_out = $("<div class='error_out'></div>")
        }

        if (isPlaying(boring_type_pupil_audio)) {
          boring_type_pupil_audio.pause();
          boring_type_pupil_audio.currentTime = 0;
        }

        if (isPlaying(pupil_music_audio)) {
          pupil_music_audio.pause();
          pupil_music_audio.currentTime = 0;
        }

        if (data["index"] !== undefined) {
          current_out_index = data["index"];
        }

        if (data["index"] === 0) {
          //arrow point
          //loop sound
          var width = 100;
          var height = 100;

          //angle calculation
          // var p1x = last_inside_x - (window.screenX - window.scrollX);
          // var p1y = last_inside_y - (window.screenY - window.scrollY + menuHeight);
          // var p2x = data["x"] - (window.screenX - window.scrollX);
          // var p2y = data["y"] - (window.screenY - window.scrollY + menuHeight);
          // var angle = Math.atan((p1y - p2y) / (p2x - p1x)) * (180 / Math.PI);
          // // console.log(angle);
          // if (p2x - p1x > 0 && p2y - p1y > 0) {
          //   angle = 180 - angle;
          //   // console.log('4');
          // } else if (p2x - p1x <= 0 && p2y - p1y <= 0) {
          //   angle = - angle;
          //   // console.log('2');
          // } else if (p2x - p1x <= 0 && p2y - p1y > 0) {
          //   angle = - angle
          //   // console.log('3');
          // } else if (p2x - p1x > 0 && p2y - p1y <= 0) {
          //   angle = 180 - angle;
          //   // console.log('1');
          // }

          error_out.css({
            position: 'absolute',
            left: - (window.screenX - window.scrollX) + data["x"] - width/2,
            top: - (window.screenY - window.scrollY + menuHeight) + data["y"] - height/2,
            width: width,
            height: height,
            "background-image": 'url("'+base_url+'eye.gif")',
            "background-position": "center",
            "background-repeat": "no-repeat",
            "background-size": "contain",
            // transform: "rotate(" + angle + "deg)",
            // "-webkit-transform": "rotate(" + angle + "deg)",
            // "-moz-transform": "rotate(" + angle + "deg)",
            // "-ms-transform": "rotate(" + angle + "deg)",
            // "-o-transform": "rotate(" + angle + "deg)"
          });
          $('body').append(error_out);

          if (!isPlaying(boring_type_gaze_audio)) {
            boring_type_gaze_audio.play();
          }
        } else if (data["index"] === 1) {
          var width = 100;
          var height = 100;
          error_out.css({
            position: 'absolute',
            left: - (window.screenX - window.scrollX) + last_inside_x - width/2,
            top: - (window.screenY - window.scrollY + menuHeight) + last_inside_y - height/2,
            width: width,
            height: height,
            "background-image": 'url("'+base_url+'pupil.gif")',
            "background-position": "center",
            "background-repeat": "no-repeat",
            "background-size": "contain",
          });
          $('body').append(error_out);

          if (!isPlaying(boring_type_gaze_audio)) {
            boring_type_gaze_audio.play();
          }
        } else if (data["index"] === 2) {
          if (!isPlaying(gaze_music_audio)) {
            gaze_music_audio.play();
          }
        } else {
          if (current_out_index === 0) {
            var width = 100;
            var height = 100;

            //angle calculation
            // var p1x = last_inside_x - (window.screenX - window.scrollX);
            // var p1y = last_inside_y - (window.screenY - window.scrollY + menuHeight);
            // var p2x = data["x"] - (window.screenX - window.scrollX);
            // var p2y = data["y"] - (window.screenY - window.scrollY + menuHeight);
            // var angle = Math.atan((p1y - p2y) / (p2x - p1x)) * (180 / Math.PI);
            // // console.log(angle);
            // if (p2x - p1x > 0 && p2y - p1y > 0) {
            //   angle = 180 - angle;
            //   // console.log('4');
            // } else if (p2x - p1x <= 0 && p2y - p1y <= 0) {
            //   angle = - angle;
            //   // console.log('2');
            // } else if (p2x - p1x <= 0 && p2y - p1y > 0) {
            //   angle = - angle
            //   // console.log('3');
            // } else if (p2x - p1x > 0 && p2y - p1y <= 0) {
            //   angle = 180 - angle;
            //   // console.log('1');
            // }

            error_out.css({
              left: - (window.screenX - window.scrollX) + data["x"] - width/2,
              top: - (window.screenY - window.scrollY + menuHeight) + data["y"] - height/2,
              // transform: "rotate(" + angle + "deg)",
              // "-webkit-transform": "rotate(" + angle + "deg)",
              // "-moz-transform": "rotate(" + angle + "deg)",
              // "-ms-transform": "rotate(" + angle + "deg)",
              // "-o-transform": "rotate(" + angle + "deg)"
            });
            $('body').append(error_out);
          } else if (current_out_index === 1) {

          } else if (current_out_index === 2) {

          }
        }

      } else if (data["boring_type"] == 'pupil') {
        if (error_out.length != 0) {
          error_out.remove();
        }

        if (isPlaying(boring_type_gaze_audio)) {
          boring_type_gaze_audio.pause();
          boring_type_gaze_audio.currentTime = 0;
        }

        if (isPlaying(gaze_music_audio)) {
          gaze_music_audio.pause();
          gaze_music_audio.currentTime = 0;
        }

        if (error_in.length == 0) {
          error_in = $('<div class="error_in"></div>');
        }

        if (data["index"] !== undefined) {
          current_in_index = data["index"];
        }

        if (data["index"] === 0) {
          //dialog
          var $div = $(outerTarget);
          $('<p>' + pupil_error_text + '</p>').dialog({
            title: "Please concentrate",
            position: {
              my: 'center',
              at: 'center',
              of: $div
            },
            modal: true,
            draggable: false,
            dialogClass: "no-close",
            buttons: [ { height: "50", width: "240", text: ok_text, click: function() { 
              $( this ).dialog( "close" );
              boring_type_pupil_audio.pause();
              boring_type_pupil_audio.currentTime = 0;
            }}]
          });

          //loop sound
          if (!isPlaying(boring_type_pupil_audio)) {
            boring_type_pupil_audio.play();
          }
        } else if (data["index"] === 1) {
          var width = 100;
          var height = 100;
          error_in.css({
            position: 'absolute',
            left: - (window.screenX - window.scrollX) + last_inside_x - width/2,
            top: - (window.screenY - window.scrollY + menuHeight) + last_inside_y - height/2,
            width: width,
            height: height,
            animation: "blink 1s steps(5, start) infinite",
            "-webkit-animation": "blink 1s steps(5, start) infinite",
            "background": "yellow",
            "-moz-border-radius": "50px",
            "-webkit-border-radius": "50px",
            "border-radius": "50px",
          });
          $('body').append(error_in);

          if (!isPlaying(boring_type_pupil_audio)) {
            boring_type_pupil_audio.play();
          }
        } else if (data["index"] === 2) {
          if (!isPlaying(pupil_music_audio)) {
            pupil_music_audio.play();
          }
        } else {
          
        }
      } else if (data["boring_type"] == 'none') {
        if (error_out.length != 0) {
          error_out.remove();
        }
        
        if (error_in.length != 0) {
          error_in.remove();
        }

        boring_type_gaze_audio.pause();
        boring_type_gaze_audio.currentTime = 0;
        boring_type_pupil_audio.pause();
        boring_type_pupil_audio.currentTime = 0;
        gaze_music_audio.pause();
        gaze_music_audio.currentTime = 0;
      }
    }
  };

  ws.onopen = function(){
    console.log("Succeeded to open a connection");

    sendBrowserData();

    if(use_mouse) {
      $(document).on('mousemove', function(e) {
        var x = window.screenX - window.scrollX + e.pageX;
        var y = window.screenY - window.scrollY + menuHeight + e.pageY;
        ws.send(JSON.stringify({click:"0,0,0,0,0,0,0,0,0,0," + x + "," + y + ",4,0,0,0,0,0,0,0,0," + x + "," + y + ",4,0,0,0,1280,960"}));
      });
    }

    setTimeout(function() {
      scroll(0, $(outerTarget).offset().top);

      var $div = $(outerTarget);
      $('<p>Press OK to start calibrating eye pupil (' + (calibration_time / 1000) + ' seconds)</p>').dialog({
        title: "Hello! " + username,
        position: {
          my: 'center',
          at: 'center',
          of: $div
        },
        modal: true,
        draggable: false,
        dialogClass: "no-close",
        buttons: [ { height: "50", width: "240", text: ok_text, click: function() { 
          $( this ).dialog( "close" );
          ws.send(JSON.stringify({calibrate: true, username: username, title: title}));
          setTimeout(function() {
            ws.send(JSON.stringify({calibrate: false}));
          }, calibration_time);
        }}]
      });
    }, 2000);
  };


  (function(){
    window.onresize = window.onscroll = sendBrowserData;

    var oldX = window.screenX,
        oldY = window.screenY;

    var interval = setInterval(function(){
      if(oldX != window.screenX || oldY != window.screenY){
        sendBrowserData();
      }

      oldX = window.screenX;
      oldY = window.screenY;
    }, 500);
  })();
});