#!/usr/bin/env ruby
require 'rubygems'
require 'eventmachine'
require 'em-websocket'
require 'date'
require 'time'
require 'active_record'

def prompt(*args)
    print(*args)
    gets
end

$out_fixation_duration = prompt("Fixation duration (Default = 0.6): ").to_f || 0.6
$out_fixation_duration = 0.6 if $out_fixation_duration == 0.0
puts "= #{$out_fixation_duration}"
$pupil_diameter_ratio = prompt("Pupil diameter ratio (Default = 0.98): ").to_f || 0.98
$pupil_diameter_ratio = 0.98 if $pupil_diameter_ratio == 0.0
puts "= #{$pupil_diameter_ratio}"
$data_skip_between = prompt("Amount of eyetracking data to skip every 1 data (No skip = 0): ").to_i
puts "= #{$data_skip_between}"
$delay_after_calibration = prompt("Delay duration after eye pupil calibration in seconds (Default = 0): ").to_i
puts "= #{$delay_after_calibration}"
$pupil_diameter_process_interval = prompt("Pupil diameter process interval in seconds (Default = 3.0): ").to_f
$pupil_diameter_process_interval = 3.0 if $pupil_diameter_process_interval == 0.0
puts "= #{$pupil_diameter_process_interval}"
$enable_out_caution = []
$enable_in_caution = []
enable_out_caution_1 = prompt("Enable out error caution 2 (Animation inside area + sound) y/n? (Default = y): ").to_s.chomp!
enable_in_caution_1 = prompt("Enable in error caution 2 (Blinking animation + sound) y/n? (Default = y): ").to_s.chomp!

if enable_out_caution_1.downcase != "n"
  $enable_out_caution << 1 
end

if enable_in_caution_1.downcase != "n"
  $enable_in_caution << 1 
end

$last_data_interval = prompt("Last inside data interval in milliseconds (Default = 0): ").to_i || 0
puts "= #{$last_data_interval}"
$process_interval = prompt("Process interval in seconds (Default = 3.0): ").to_f || 3.0
$process_interval = 3.0 if $process_interval == 0.0
puts "= #{$process_interval}"

class Main
  def initialize
    options = {
      process_interval: $process_interval
    }
    $eye_track = EyeTrack.new options
    $connections = {}
  end

  def run
    EM.run do
      udp_port = 9000
      websocket_port = 9001

      EM.open_datagram_socket('0.0.0.0', udp_port, UDPHandler)
      puts "Opened UDP server with port #{udp_port}"
      puts "Waiting for browser connection (Refresh if already open)"
       
      index = 1
       
      def login_names(connections)
        connections.map {|ws, name| name}.join(", ")
      end
       
      EM::WebSocket.run(:host => "0.0.0.0", :port => websocket_port) do |ws|    
        ws.onopen do |handshake|
          puts "New WebSocket connection"
          name  = "Guest#{index}"
          $connections[ws] = name
          index += 1
     
          ws.send "[Server] Hello #{name}"
          ws.send "[Server] Members are: #{login_names($connections)}"
        end
     
        ws.onclose do
          puts "WebSocket connection closed"
          $eye_track.unset_browser_data
          $connections.delete(ws)
        end
     
        ws.onmessage do |msg|
          browser_data = JSON.parse msg
          sender = $connections[ws]
          
          if browser_data["click"]
            command = "#{Time.now.to_f},#{browser_data["click"]}"
            
            $eye_track.save(command)
          elsif browser_data["calibrate"] == true
            $eye_track.set_username browser_data["username"]
            $eye_track.set_title browser_data["title"]
            $eye_track.start_calibrate
          elsif browser_data["calibrate"] == false
            $eye_track.stop_calibrate

            puts "Delay #{$delay_after_calibration} seconds"
            $stdout.flush
            sleep $delay_after_calibration
            puts "Delay complete"
          else
            $eye_track.set_browser_data browser_data
          end
        end
      end
      puts "Opened WebSocket with port #{websocket_port}"
    end
  end
end

class EyetrackData < ActiveRecord::Base  
end  

class EyeTrack
  def initialize(options = {})
    $session = DateTime.now.strftime "%Y%m%d%H%M%S"
    @username = "Anonymous"
    @title = ""
    @total_in = 0
    @total_out = 0
    @fixation_ratio = 1
    @pupil_boring_mode = $enable_in_caution.dup
    @gaze_boring_mode = $enable_out_caution.dup
    @calibrating = true

    @in_x = nil
    @in_y = nil
    @in_w = nil
    @in_h = nil

    @latest = {}

    @last_boring_type = "none"
    @state_change_timestamp = Time.now

    @process_interval = options[:process_interval] || 60

    @db_name = "eyetrack"

    @skip_data_counter = 0

    # @db_client = Mysql2::Client.new(:host => "localhost", :username => "root", :database => @db_name, :port => 3306)
    ActiveRecord::Base.establish_connection(  
      :adapter => "mysql2",  
      :host => "127.0.0.1",
      :port => 3306,
      :database => @db_name
    )  
  end

  def save(command)
    unless @browser_data_set
      puts "Browser data not set"
      return
    end

    if $data_skip_between != 0
      if @skip_data_counter == 0
        @skip_data_counter += 1
      elsif @skip_data_counter < $data_skip_between
        # puts "Skip data"
        @skip_data_counter += 1
        return
      elsif @skip_data_counter == $data_skip_between
        # puts "Skip data"
        @skip_data_counter = 0
        return
      end
    end

    # puts "Use data"

    timestamp, dataid, reserved, time, time_internal, l_pupilx, l_pupily, l_glint1x, l_glint1y, l_glint2x, l_glint2y, l_gazex, l_gazey, l_pupildiameter, l_found, l_calibrated, r_pupilx, r_pupily , r_glint1x, r_glint1y, r_glint2x , r_glint2y, r_gazex , r_gazey , r_pupildiameter , r_found , r_calibrated, pixeldata, width, height = command.split(",")

    pupildiameter_bar = (l_pupildiameter.to_f + r_pupildiameter.to_f) / 2
    if @calibrating
      if @pupil_diameter.nil?
        @pupil_diameter = pupildiameter_bar
      else
        @pupil_diameter = (pupildiameter_bar + @pupil_diameter) / 2
      end
      @pupil_boring_mode = $enable_in_caution.dup
      @gaze_boring_mode = $enable_out_caution.dup
      return
    end

    if @pupil_diameter.nil?
      @pupil_diameter = pupildiameter_bar
    end


    data = {
      timestamp: timestamp, #use
      dataid: dataid,
      reserved: reserved,
      time: time,
      time_internal: time_internal,
      l_pupilx: l_pupilx,
      l_pupily: l_pupily,
      l_glint1x: l_glint1x,
      l_glint1y: l_glint1y,
      l_glint2x: l_glint2x,
      l_glint2y: l_glint2y,
      l_gazex: l_gazex, #use
      l_gazey: l_gazey, #use
      l_pupildiameter: l_pupildiameter, #use
      l_found: l_found, #use
      l_calibrated: l_calibrated,
      r_pupilx: r_pupilx,
      r_pupily: r_pupily,
      r_glint1x: r_glint1x,
      r_glint1y: r_glint1y,
      r_glint2x: r_glint2x,
      r_glint2y: r_glint2y,
      r_gazex: r_gazex, #use
      r_gazey: r_gazey, #use
      r_pupildiameter: r_pupildiameter, #use
      r_found: r_found, #use
      r_calibrated: r_calibrated,
      pixeldata: pixeldata,
      width: width,
      height: height
    }

    data[:gazex_bar] = (data[:l_gazex].to_i + data[:r_gazex].to_i) / 2
    data[:gazey_bar] = (data[:l_gazey].to_i + data[:r_gazey].to_i) / 2

    data[:gazex_bar] = 10 if data[:gazex_bar] < 10
    data[:gazex_bar] = 1670 if data[:gazex_bar] > 1670
    data[:gazey_bar] = 10 if data[:gazey_bar] < 10
    data[:gazey_bar] = 1040 if data[:gazey_bar] > 1040

    data[:pupildiameter_bar] = pupildiameter_bar
    data[:ignore] = !(data[:l_found] && data[:r_found])
    data[:in_x] = @in_x
    data[:in_y] = @in_y
    data[:in_w] = @in_w
    data[:in_h] = @in_h

    @latest = {
      x: data[:gazex_bar],
      y: data[:gazey_bar],
      ignored: data[:ignore]
    }

    puts @latest

    EyetrackData.create data

    self.process
  end

  def set_browser_data(browser_data)
    @in_x = browser_data["x"]
    @in_y = browser_data["y"]
    @in_w = browser_data["w"]
    @in_h = browser_data["h"]
    @browser_data_set = true
    puts "browser data set : #{browser_data}"
  end

  def unset_browser_data
    @browser_data_set = false
    @pupil_diameter = nil
    puts "browser data unset"
  end

  def process
    unless @browser_data_set
      puts "Browser data not set"
      return
    end

    if @calibrating
      puts "Calibrating"
      return
    end

    sum_time_in = sum_time_out = 0.000001
    sum_pupil_diameter_in = sum_pupil_diameter_out = 0
    data_count_in = data_count_out = 0

    data_array = EyetrackData.order('timestamp ASC').where(['timestamp > ?', Time.now.to_f - @process_interval]).to_a

    return if data_array.count < 2

    sum_time_out = 0
    data_array_reverse = data_array.reverse
    latest_data = data_array_reverse.first

    data_array_count = data_array.count
    data_array_reverse.each_with_index do |data, index|
      if index == data_array_count - 1 || data[:ignore]
        next
      end

      break if inside?(data[:gazex_bar], data[:gazey_bar], data[:in_x], data[:in_y], data[:in_w], data[:in_h])
      time_interval = data_array_reverse[index][:timestamp] - data_array_reverse[index + 1][:timestamp]
      sum_time_out += time_interval
    end

    # Pupil diameter process interval
    calculated_pupil_diameter = 0

    if $pupil_diameter_process_interval > 0
      pupil_data_array = EyetrackData.order('timestamp DESC').where(['timestamp > ?', Time.now.to_f - $pupil_diameter_process_interval]).to_a

      if pupil_data_array.count > 0
        sum_pupil_diameter = 0.0
        pupil_data_array.each do |data|
          sum_pupil_diameter += data[:pupildiameter_bar]
        end
        calculated_pupil_diameter = sum_pupil_diameter / pupil_data_array.count
      end

      puts "pupil_diameter : #{calculated_pupil_diameter} (avg from #{pupil_data_array.count} data)"
    else
      puts "pupil_diameter : #{calculated_pupil_diameter} last data"
    end

    still_outside_and_out_error_state = !inside?(latest_data[:gazex_bar], latest_data[:gazey_bar], latest_data[:in_x], latest_data[:in_y], latest_data[:in_w], latest_data[:in_h]) && @last_boring_type == "gaze"

    if still_outside_and_out_error_state || (sum_time_out > $out_fixation_duration)
      trigger_data = {
        boring_type: "gaze",
        x: latest_data[:gazex_bar],
        y: latest_data[:gazey_bar]
      }
    elsif calculated_pupil_diameter > @pupil_diameter / $pupil_diameter_ratio
      trigger_data = {
        boring_type: "pupil"
      }  
    else
      trigger_data = {
        boring_type: "none"
      }
    end

    timestamp = Time.now

    inside_margin = 200
    # calculate last inside data in last x milliseconds
    last_x_milliseconds = EyetrackData.order('timestamp ASC').where(['timestamp > ?', Time.now.to_f - $last_data_interval / 1000.0]).to_a

    found_latest_x_inside_data = false
    last_x_milliseconds.each do |data|
      if inside?(data[:gazex_bar], data[:gazey_bar], data[:in_x] + inside_margin, data[:in_y] + inside_margin, data[:in_w] - inside_margin*2, data[:in_h] - inside_margin*2)
        @last_inside_data = { 
          x: data[:gazex_bar],
          y: data[:gazey_bar]
        }
        found_latest_x_inside_data = true
        break
      end
    end

    if !found_latest_x_inside_data && inside?(latest_data[:gazex_bar], latest_data[:gazey_bar], latest_data[:in_x] + inside_margin, latest_data[:in_y] + inside_margin, latest_data[:in_w] - inside_margin*2, latest_data[:in_h] - inside_margin*2)
      @last_inside_data = { 
        x: latest_data[:gazex_bar],
        y: latest_data[:gazey_bar]
      }
    end

    if @last_boring_type != trigger_data[:boring_type]
      boring_type_index = 'none'
      if trigger_data[:boring_type] == "gaze" && @gaze_boring_mode.length > 0
        boring_type_index = @gaze_boring_mode.first
        trigger_data[:index] = boring_type_index
        @gaze_boring_mode.push @gaze_boring_mode.shift

        if @last_inside_data
          trigger_data[:last_inside_x] = @last_inside_data[:x]
          trigger_data[:last_inside_y] = @last_inside_data[:y]
        end
      elsif trigger_data[:boring_type] == "pupil"
        boring_type_index = @pupil_boring_mode.first
        trigger_data[:index] = boring_type_index
        @pupil_boring_mode.push @pupil_boring_mode.shift

        if @last_inside_data
          trigger_data[:last_inside_x] = @last_inside_data[:x]
          trigger_data[:last_inside_y] = @last_inside_data[:y]
        end
      end 
      # username,title,pupil_diameter_standard,pupil_diameter,x,y,time_internal,timestamp,state_from,state_to,boring_type_index,time_elapsed
      csv = "#{@username},#{@title},#{@pupil_diameter},#{calculated_pupil_diameter},#{latest_data[:gazex_bar]},#{latest_data[:gazey_bar]},#{latest_data[:time_internal]},#{timestamp.strftime('%F %T.%6N')},#{@last_boring_type},#{trigger_data[:boring_type]},#{boring_type_index},#{timestamp.to_f - @state_change_timestamp.to_f}"
      File.open("data/#{$session}.txt", "a") do |f|
        f.puts csv
      end
      @last_boring_type = trigger_data[:boring_type]
      @state_change_timestamp = timestamp
    end

    $connections.each do |cws, name|
      cws.send "#{trigger_data.to_json}"
    end

    puts "Out fixation duration : #{sum_time_out}"
    puts "Boring type : #{trigger_data[:boring_type]}"
  end

  def inside?(data_x, data_y, in_x, in_y, in_w, in_h)
    data_x >= in_x && data_x <= in_x + in_w && data_y >= in_y && data_y <= in_y + in_h
  end

  def set_username(username)
    @username = username 
  end

  def set_title(title)
    @title = title 
  end

  def start_calibrate
    puts "Start calibrating"    
    @calibrating = true
  end

  def stop_calibrate
    puts "Stop calibrating"    

    csv = "username,title,pupil_diameter_standard,pupil_diameter,x,y,time_internal,timestamp,state_from,state_to,boring_type_index,time_elapsed"
    timestamp = Time.now
    csv2 = "#{@username},#{@title},#{@pupil_diameter},,,,,#{timestamp.strftime('%F %T.%6N')},none,none,,"
    File.open("data/#{$session}.txt", "a") do |f|
      f.puts csv
      f.puts csv2
    end
    
    @calibrating = false
  end
end
 
class UDPHandler < EM::Connection
  def initialize
  end

  def receive_data(command)
    command.chomp!
    # log("Received #{command}")
    command = "#{Time.now.to_f},#{command}"

    $eye_track.save(command)
  end
 
private
  def callback
    EM::DefaultDeferrable.new.callback do |response|
      send_data(response + "\n")
      log(response)
    end
  end
 
  def log(message)
    puts "#{DateTime.now.to_s} : #{message}"
  end
end

main = Main.new
main.run