-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 23, 2014 at 03:56 AM
-- Server version: 5.6.12
-- PHP Version: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `eyetrack`
--
CREATE DATABASE IF NOT EXISTS `eyetrack` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eyetrack`;

-- --------------------------------------------------------

--
-- Table structure for table `eyetrack_data`
--

CREATE TABLE IF NOT EXISTS `eyetrack_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` double NOT NULL,
  `dataid` bigint(20) NOT NULL,
  `reserved` bigint(20) NOT NULL,
  `time` bigint(20) NOT NULL,
  `time_internal` double NOT NULL,
  `l_pupilx` double NOT NULL,
  `l_pupily` double NOT NULL,
  `l_glint1x` double NOT NULL,
  `l_glint1y` double NOT NULL,
  `l_glint2x` double NOT NULL,
  `l_glint2y` double NOT NULL,
  `l_gazex` int(11) NOT NULL,
  `l_gazey` int(11) NOT NULL,
  `l_pupildiameter` double NOT NULL,
  `l_found` tinyint(1) NOT NULL,
  `l_calibrated` tinyint(1) NOT NULL,
  `r_pupilx` double NOT NULL,
  `r_pupily` double NOT NULL,
  `r_glint1x` double NOT NULL,
  `r_glint1y` double NOT NULL,
  `r_glint2x` double NOT NULL,
  `r_glint2y` double NOT NULL,
  `r_gazex` int(11) NOT NULL,
  `r_gazey` int(11) NOT NULL,
  `r_pupildiameter` double NOT NULL,
  `r_found` tinyint(1) NOT NULL,
  `r_calibrated` tinyint(1) NOT NULL,
  `pixeldata` bigint(20) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `gazex_bar` double NOT NULL,
  `gazey_bar` double NOT NULL,
  `ignore` tinyint(1) NOT NULL,
  `pupildiameter_bar` double NOT NULL,
  `in_x` int(11) NOT NULL,
  `in_y` int(11) NOT NULL,
  `in_w` int(11) NOT NULL,
  `in_h` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13213 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
